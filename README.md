# COVID data

## Memory-optimized reading of Excel files

Built-in Pandas method uses a lot of memory for large files. We try to limit
memory usage by reading one row at a time and filtering only required columns,
using methods from `openpyxl` package. It does help to an extend, but reading
XLSX files remains a memory intensive process.

Run docker with memory limit to see whether it will work:

```bash
docker build -t covid_data:latest .
docker run --memory="256m" covid_data:latest
```