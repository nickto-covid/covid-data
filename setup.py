from setuptools import setup, find_packages

setup(name="covid_data",
      version="0.0.1",
      packages=find_packages(),
      python_requires=">=3",
      install_requires=[
          "pandas>=1.3.1",
          "requests>=2.26.0",
          "openpyxl>=3.0.7",
          "scipy>=1.7.1",
      ])