import datetime
import json
import logging
import os
import re
import urllib

import numpy as np
import pandas as pd
import requests
from dateutil import tz

from .resource import Resource

logger = logging.getLogger(__name__)
logging.basicConfig(format=os.getenv("LOG_FORMAT", "%(asctime)s - %(name)s - %(levelname)s - %(message)s"))  # yapf: disable
logger.setLevel(level=int(os.getenv("LOG_LEVEL", logging.DEBUG)))


class Population(Resource):
    FILENAMES = {
        "source": "population.csv",
        "preprocessed": "preprocessed.pkl",
    }

    def __init__(self, path=None, logger=logger):
        super().__init__(path=path)

        self.last_modified = None
        self.logger = logger

    def download(self):
        req = urllib.request.Request(
            url=
            "https://data.stat.gov.lv:443/api/v1/lv/OSP_PUB/START/POP/IR/IRS/IRS010m",
            data=json.dumps({
                "query": [{
                    "code": "ContentsCode",
                    "selection": {
                        "filter": "item",
                        "values": ["IRS010m"]
                    }
                },],
                "response": {
                    "format": "csv"
                }
            }).encode("utf-8"),
        )
        # yapf: disable
        df = (
            pd.read_csv(
                urllib.request.urlopen(req),
                sep=",",
                na_values="…",
            )
            .transpose()
            .iloc[1:]
            .reset_index()
        )
        # yapf: enable
        df.columns = ["date", "population"]

        path = os.path.join(self.path, self.FILENAMES["source"])
        df.to_csv(path, index=False)
        return path

    def get_last_modified(self):
        url = "https://stat.gov.lv/en/statistics-themes/population/population-number/tables/irs010m-population-and-key-vital-statistics"
        response = requests.get(url)
        html = response.text
        # We are looking for a string like this:
        # <dt>Data updated</dt><dd><time dateTime="1628848800000" class="">13.08.2021</time></dd>
        datetime_tags = re.findall(r"\<dt\>Data updated.*?\<\/dd\>", html)[0]
        timestamp = re.findall(r"\d{13,}", datetime_tags)[0]
        date_str = re.findall(r"\d\d\.\d\d\.\d\d\d\d", datetime_tags)[0]
        modified_datetime = datetime.datetime.utcfromtimestamp(
            int(timestamp) // 10e2)
        modified_date = datetime.datetime.strptime(date_str, "%d.%m.%Y")
        assert modified_datetime.date() == modified_date.date()

        last_modified = modified_datetime
        last_modified = last_modified.replace(tzinfo=tz.gettz("Europe/Riga"))
        return last_modified

    def read(self) -> pd.DataFrame:
        path = os.path.join(self.path, self.FILENAMES["source"])
        if not os.path.exists(path):
            raise RuntimeError("You should download data first.")

        self.data = pd.read_csv(path)
        self.preprocessed = False
        return self.data

    def preprocess(self) -> pd.DataFrame:
        if self.data is None:
            raise RuntimeError("You should read the data first.")

        # Set index as date
        df = self.data
        df.loc[:, "date"] = pd.to_datetime(df.loc[:, "date"], format="%YM%m")
        df = df.set_index(
            pd.date_range(
                df.loc[:, "date"].min(),
                df.loc[:, "date"].max(),
                freq="MS",
            ))
        df = df.drop("date", axis=1)

        # Fix population
        df.loc[:, "population"] = df.loc[:, "population"].astype(float)
        df.loc[:, "population"] = df.loc[:, "population"] * 1000
        df = df.fillna(method="backfill")

        df = df.reset_index()
        df = df.rename({"index": "date"}, axis=1)

        # Cast types
        df.loc[:, "population"] = df.loc[:, "population"].astype(int)

        self.data = df
        self.preprocessed = True
        return self.data

    def interpolate(self):
        if not self.preprocessed:
            raise RuntimeError("Should preprocess data first.")

        # Create daily data frame without missing dates
        df = pd.DataFrame({
            "date":
                pd.date_range(
                    self.data.loc[:, "date"].min(),
                    self.data.loc[:, "date"].max(),
                    freq="D",
                )
        }).set_index("date")
        # Fill it in
        df = df.join(self.data.set_index("date"), how="left")

        # Interpolate
        df = (df.loc[:, "population"].interpolate(method="polynomial",
                                                  order=2).reset_index())

        # Make sure population is ints
        df.loc[:, "population"] = df.loc[:, "population"].astype(np.int64)

        return df

    def get_hospitalized_by_date(self) -> pd.DataFrame:
        if not self.preprocessed:
            raise RuntimeError("Should preprocess data first.")

        df = self.data.copy()

        # yapf: disable
        df = (
            df
            .groupby(["date"])
            .agg(
                hospitalized_count=("hospitalized_count", np.sum),
                hospitalized_severe_count=("hospitalized_severe_count", np.sum),
                hospitalized_moderate_count=("hospitalized_moderate_count", np.sum),
                discharged_cumcount=("discharged_cumcount", np.sum),
            ).reset_index()
        )
        # yapf: enable

        # Make sure we have all the dates
        df = df.set_index("date")
        df = df.reindex(pd.date_range(df.index.min(), df.index.max()),
                        method="backfill")
        df = df.reset_index().rename({"index": "date"}, axis=1)

        return df.sort_values("date")


def main():
    "Example usage."
    p = Population(path="/tmp/population")
    p.get_last_modified()
    p.download()
    p.read()
    p.preprocess()
    df = p.interpolate()
    print(df)


if __name__ == "__main__":
    main()
