import logging
import os

import pandas as pd
from .resource import LatvianOpenDataPortalResource

logger = logging.getLogger(__name__)
logging.basicConfig(format=os.getenv("LOG_FORMAT", "%(asctime)s - %(name)s - %(levelname)s - %(message)s"))  # yapf: disable
logger.setLevel(level=int(os.getenv("LOG_LEVEL", logging.DEBUG)))


class Cases(LatvianOpenDataPortalResource):
    LV_OPEN_DATA_RESOURCE_ID = "covid-19"

    FILENAMES = {
        "source": "cases.csv",
        "preprocessed": "preprocessed.pkl",
    }
    # yapf: disable
    COLUMN_TRANSLATION = {
        "Datums": "date",
        "TestuSkaits": "tests_count",
        "ApstiprinataCOVID19InfekcijaSkaits": "cases_count",
        "Ipatsvars": "positivity_rate",
        "IzarstetoPacientuSkaits": "recovered_confirmed_count",  # following some algorithm
        "MirusoPersonuSkaits": "deaths_count",
        "ApstiprinatiVecGr_0-9Gadi": "cases_0y_to_9y",
        "ApstiprinatiVecGr_10-19Gadi": "cases_10y_to_19y",
        "ApstiprinatiVecGr_20-29Gadi": "cases_20y_to_29y",
        "ApstiprinatiVecGr_30-39Gadi": "cases_30y_to_39y",
        "ApstiprinatiVecGr_40-49Gadi": "cases_40y_to_49y",
        "ApstiprinatiVecGr_50-59Gadi": "cases_50y_to_59y",
        "ApstiprinatiVecGr_60-69Gadi": "cases_60y_to_69y",
        "ApstiprinatiVecGr_70GadiUnVairak": "cases_70y_and_older",
        "ApstiprinatiVecGr_70-79Gadi": "cases_70y_to_79y",
        "ApstiprinatiVecGr_80GadiUnVairak": "cases_80y_and_older",
        "IzveselojusosSkaits": "recovered_14_days_count",  # 14 days after confirmed
        "14DienuKumulativaSaslimstibaUz100000Iedzivotaju": "cases_14_days_cumcount",
        "ApstCOVID19InfSk_NevakcVakcNepab": "cases_not_fully_vaccinated_count",
        "ApstCOVID19InfSk_Vakc": "cases_fully_vaccinated_count",
        "ApstCOVID19InfSk_VakcNepab": "cases_partially_vaccinated_count",
        "ApstCOVID19InfSk_Nevakc": "cases_not_vaccinated_count",
        "MirusoPersonuSkaits_NevakcVakcNepab": "deaths_not_fully_vaccinated_count",
        "MirusoPersonuSkaits_Vakc": "deaths_fully_vaccinated_count",
    }
    # yapf: enable

    def __init__(self, path=None, logger=logger):
        super().__init__(path=path)
        self.logger = logger

    def get_urls(self) -> str:
        return self._get_urls(id=self.LV_OPEN_DATA_RESOURCE_ID)

    def get_last_modified(self):
        return self._get_last_modified(id=self.LV_OPEN_DATA_RESOURCE_ID)

    def _read_single_file(self, path, columns=None):
        if not os.path.exists(path):
            raise RuntimeError("You should download data first.")

        return pd.read_csv(
            path,
            usecols=columns,
            encoding="WINDOWS-1252",
            sep=";",
            quotechar="\"",
            na_values=["...", "…"],
        )

    def read(self, columns=None):
        if self.source_paths is None or len(self.source_paths) == 0:
            raise RuntimeError("You should download data first.")

        self.data = []
        for path in self.source_paths:
            self.data.append(self._read_single_file(path, columns))
        self.data = pd.concat(self.data, axis=0)

        self.preprocessed = False
        return self.data

    def preprocess(self):
        # Fix and subset columns
        self.data.columns = [c.strip() for c in self.data.columns]
        self.data = self.data.rename(self.COLUMN_TRANSLATION, axis=1)
        self.data.loc[:, "date"] = pd.to_datetime(self.data.loc[:, "date"])

        path = os.path.join(self.path, self.FILENAMES["preprocessed"])
        self.data.to_pickle(path)
        self.preprocessed = True
        return self.data


def main():
    c = Cases("/tmp/data/cases")
    c.get_urls()
    c.download()
    df = c.read()
    df = c.preprocess()
    print(df)
    print(df.dtypes)
    return


if __name__ == "__main__":
    main()