import logging
import os
from typing import List

import numpy as np
import pandas as pd

from .resource import LatvianOpenDataPortalResource

logger = logging.getLogger(__name__)
logging.basicConfig(format=os.getenv("LOG_FORMAT", "%(asctime)s - %(name)s - %(levelname)s - %(message)s"))  # yapf: disable
logger.setLevel(level=int(os.getenv("LOG_LEVEL", logging.DEBUG)))


class Hospitalizations(LatvianOpenDataPortalResource):
    LV_OPEN_DATA_RESOURCE_ID = "stacionaru-operativie-dati-par-covid19"

    FILENAMES = {
        "source": "hospitalizations.xlsx",
        "preprocessed": "preprocessed.pkl",
    }
    COLUMN_TRANSLATION = {
        "Atskaites datums": "date",
        "AI kods": "hospital_code",
        "AI nosaukums": "hospital_name",
        "Pacienti stacionārā": "hospitalized_count",
        "Pacienti ar smagu slimība gaitu": "hospitalized_severe_count",
        "Pacienti ar vidēju slimība gaitu": "hospitalized_moderate_count",
        "Izrakstītie": "discharged_cumcount",
    }

    def __init__(self, path=None, logger=logger):
        super().__init__(path=path)
        self.logger = logger

    def get_urls(self) -> str:
        return self._get_urls(id=self.LV_OPEN_DATA_RESOURCE_ID)

    def get_last_modified(self):
        return self._get_last_modified(id=self.LV_OPEN_DATA_RESOURCE_ID)

    def _read(self,
              path: str,
              columns: List[str] = None,
              memory_optimized: bool = True,
              top: int = None) -> pd.DataFrame:
        extension = os.path.splitext(path)[-1].lower()
        assert extension.lower() == ".xlsx"  # otherwise something changed

        if memory_optimized:
            self.logger.debug("Reading using memory optimized method.")
            df = self._read_excel_memory_optimized(
                path,
                sheet="STAC_COVID",
                columns=columns,
                top=top,
            )
        else:
            self.logger.debug("Reading using built-in Pandas (not memory optimized) method.")  # yapf: disable
            df = pd.read_excel(
                path,
                sheet_name=0,
                header=0,
                usecols=columns,
            )
        return df

    def _read_single_file(self,
                          path,
                          columns: List[str] = None,
                          memory_optimized: bool = True,
                          top: int = None) -> pd.DataFrame:
        if not os.path.exists(path):
            raise RuntimeError("You should download data first.")

        return self._read(path=path,
                          columns=columns,
                          memory_optimized=memory_optimized,
                          top=top)

    def read(self,
             columns: List[str] = None,
             memory_optimized: bool = True,
             top: int = None) -> pd.DataFrame:
        if self.source_paths is None or len(self.source_paths) == 0:
            raise RuntimeError("You should download data first.")        
                         
        self.data = []
        for path in self.source_paths:
            self.data.append(
                self._read_single_file(path,
                                       columns=columns,
                                       memory_optimized=memory_optimized,
                                       top=top))
        self.data = pd.concat(self.data, axis=0)

        self.preprocessed = False
        return self.data

    def preprocess(self) -> pd.DataFrame:
        if self.data is None:
            raise RuntimeError("You should read the data first.")

        # Fix and subset columns
        self.logger.debug("Renaming and subsetting columns.")
        self.data.columns = [c.strip() for c in self.data.columns]
        self.data = self.data.rename(
            self.COLUMN_TRANSLATION,
            axis=1,
        )

        self.preprocessed = True
        return self.data

    def get_hospitalized_by_date(self) -> pd.DataFrame:
        if not self.preprocessed:
            raise RuntimeError("Should preprocess data first.")

        df = self.data.copy()

        # yapf: disable
        df = (
            df
            .groupby(["date"])
            .agg(
                hospitalized_count=("hospitalized_count", np.sum),
                hospitalized_severe_count=("hospitalized_severe_count", np.sum),
                hospitalized_moderate_count=("hospitalized_moderate_count", np.sum),
                discharged_cumcount=("discharged_cumcount", np.sum),
            ).reset_index()
        )
        # yapf: enable

        # Make sure we have all the dates
        df = df.set_index("date")
        df = df.reindex(pd.date_range(df.index.min(), df.index.max()),
                        method="backfill")
        df = df.reset_index().rename({"index": "date"}, axis=1)

        return df.sort_values("date")


def main():
    "Example usage."
    h = Hospitalizations(path="/tmp/hospitalizations")
    print(h.get_url())
    h.download()
    h.read()
    h.preprocess()
    df = h.get_hospitalized_by_date()
    print(df)
    quit()


if __name__ == "__main__":
    main()
