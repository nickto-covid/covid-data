import datetime
import os
import shutil
import tempfile
from abc import ABC, abstractmethod

import pandas as pd
import requests
from dateutil import tz
from openpyxl import load_workbook


class Resource(ABC):

    @abstractmethod
    def __init__(self, path=None):
        self.path = path if path else tempfile.mkdtemp()
        os.makedirs(self.path, exist_ok=True)
        if not os.path.isdir(self.path):
            raise ValueError("Wrong path provided: %s.", self.path)

        self.data = None
        self.preprocessed = False
        self.last_modified = None

    def __del__(self):
        if os.path.exists(self.path):
            shutil.rmtree(self.path)

    @abstractmethod
    def get_last_modified(self) -> datetime.datetime:
        return

    @abstractmethod
    def download(self, path: str) -> None:
        return

    @abstractmethod
    def read(self, path: str = None) -> pd.DataFrame:
        return

    @abstractmethod
    def preprocess(self, df: pd.DataFrame) -> pd.DataFrame:
        return

    @staticmethod
    def _read_excel_memory_optimized(path, sheet, columns=None, top=None):
        """Read .xlsx in a memory optimized way.
        
        Note, that although it uses less memory than built-in Pandas method,
        it still uses a lot of it.
        """
        wb = load_workbook(filename=path, read_only=True)
        ws = wb[sheet]

        rows = []
        for i, row in enumerate(ws.rows):
            if i == 0:
                # Header
                header = [cell.value for cell in row]
                if columns:
                    col_idx = [i for i, c in enumerate(header) if c in columns]
                else:
                    col_idx = list(range(len(header)))
                    columns = header
            else:
                # Data
                if columns:
                    values = [cell.value for cell in row]
                    values = [v for i, v in enumerate(values) if i in col_idx]
                else:
                    values = [cell.value for cell in row]
                rows.append(values)

            if top and i >= top:
                break

        wb.close()

        return pd.DataFrame(rows, columns=[header[i] for i in col_idx])


class LatvianOpenDataPortalResource(Resource):
    @abstractmethod
    def __init__(self, path=None):
        super().__init__(path=path)

        self.metadata = None
        self.urls = None
        self.source_paths = []

    @abstractmethod
    def get_urls(self) -> str:
        return

    def _get_metadata(self, id: str) -> dict:
        if self.metadata:
            return self.metadata

        response = requests.get(
            "https://data.gov.lv/dati/eng/api/3/action/package_show",
            params={"id": id})
        assert response.ok
        metadata = response.json()
        assert metadata["success"]

        self.metadata = metadata
        return self.metadata

    def _get_urls(self, id: str) -> str:
        if self.urls:
            return self.urls

        metadata = self._get_metadata(id=id)
        urls = []
        for i in range(len(metadata["result"]["resources"])):
            url = metadata["result"]["resources"][i]["url"]
            if "metadata" not in url:
                urls.append(url)

        self.urls = sorted(urls)
        return self.urls

    def _get_last_modified(self, id: str) -> datetime.datetime:
        if self.last_modified:
            return self.last_modified

        metadata = self._get_metadata(id=id)

        candidates = []
        for i in range(len(metadata["result"]["resources"])):
            last_modified = metadata["result"]["resources"][i]["last_modified"]
            last_modified = datetime.datetime.fromisoformat(last_modified)
            last_modified = last_modified.replace(tzinfo=tz.gettz("Europe/Riga"))
            candidates.append(last_modified)
        last_modified = max(candidates)

        self.last_modified = last_modified
        return self.last_modified

    def download(self) -> None:
        assert "source" in self.FILENAMES
        

        for i, url in enumerate(self.get_urls()):
            path = os.path.join(self.path, self.FILENAMES["source"])
            base, ext = os.path.splitext(path)
            path = f"{base}_{i:d}{ext}"
            self.source_paths.append(path)

            self.logger.info("Downloading from %s to %s.", url, path)
            response = requests.get(url)
            assert response.ok
            with open(path, "wb+") as f:
                f.write(response.content)
            self.logger.debug("Downloaded from %s to %s.", url, path)
        return self.source_paths
