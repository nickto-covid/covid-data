import itertools
import logging
import os
from typing import List

import numpy as np
import pandas as pd

from .resource import LatvianOpenDataPortalResource

logger = logging.getLogger(__name__)
logging.basicConfig(format=os.getenv("LOG_FORMAT", "%(asctime)s - %(name)s - %(levelname)s - %(message)s"))  # yapf: disable
logger.setLevel(level=int(os.getenv("LOG_LEVEL", logging.DEBUG)))


class Vaccination(LatvianOpenDataPortalResource):
    LV_OPEN_DATA_RESOURCE_ID = "covid19-vakcinacijas"

    FILENAMES = {
        "source": "vaccination.xlsx",
        "status": "status.json",
        "preprocessed": "preprocessed.pkl",
    }
    COLUMN_TRANSLATION = {
        "Vakcinācijas datums": "date",
        "Vakcīnas kārtas numurs": "dose",
        "Vakcinēto personu skaits": "count",
        "Preparāts": "vaccine",
    }

    def __init__(self, path="data/vaccination", logger=logger):
        super().__init__(path=path)
        self.logger = logger

    def get_urls(self) -> str:
        return self._get_urls(id=self.LV_OPEN_DATA_RESOURCE_ID)

    def get_last_modified(self):
        return self._get_last_modified(id=self.LV_OPEN_DATA_RESOURCE_ID)

    def _read(self,
              path: str,
              sheet: str,
              columns: List[str] = [
                  "Vakcinācijas datums",
                  "Vakcīnas kārtas numurs",
                  "Vakcinēto personu skaits",
                  "Preparāts",
              ],
              memory_optimized: bool = True,
              top: int = None) -> pd.DataFrame:
        extension = os.path.splitext(path)[-1].lower()
        assert extension.lower() == ".xlsx"  # otherwise something changed

        if memory_optimized:
            self.logger.debug("Reading using memory optimized method.")
            df = self._read_excel_memory_optimized(
                path,
                sheet=sheet,
                columns=columns,
                top=top,
            )
        else:
            self.logger.debug("Reading using built-in Pandas (not memory optimized) method.")  # yapf: disable
            df = pd.read_excel(
                path,
                sheet_name=0,
                header=0,
                usecols=columns,
            )
        return df

    def _read_single_file(self,
                          path: str,
                          sheet: str,
                          columns: List[str] = None,
                          memory_optimized: bool = True,
                          top: int = None) -> pd.DataFrame:
        if not os.path.exists(path):
            raise RuntimeError("You should download data first.")

        return self._read(path=path,
                          sheet=sheet,
                          columns=columns,
                          memory_optimized=memory_optimized,
                          top=top)

    def read(self,
             columns: List[str] = [
                 "Vakcinācijas datums",
                 "Vakcīnas kārtas numurs",
                 "Vakcinēto personu skaits",
                 "Preparāts",
             ],
             memory_optimized: bool = True,
             top: int = None) -> pd.DataFrame:
        if self.source_paths is None or len(self.source_paths) == 0:
            raise RuntimeError("You should download data first.")

        self.data = []
        for path in self.source_paths:
            self.data.append(
                self._read_single_file(path,
                                       sheet="Parskats ATV_data_p",
                                       columns=columns,
                                       memory_optimized=memory_optimized,
                                       top=top))
        self.data = pd.concat(self.data, axis=0)

        self.preprocessed = False
        return self.data

    def preprocess(self) -> pd.DataFrame:
        if self.data is None:
            raise RuntimeError("You should read the data first.")

        # Fix and subset columns
        self.logger.debug("Renaming and subsetting columns.")
        self.data.columns = [c.strip() for c in self.data.columns]
        self.data = self.data.rename(
            self.COLUMN_TRANSLATION,
            axis=1,
        )
        self.data = self.data.loc[:, ["date", "dose", "count", "vaccine"]]

        # # Fix dose: make it integer
        # # This works with 'Vakcinācijas posms' column, which has the following
        # # format: '1.pote'. But there has been a new column introduced,
        # # 'Vakcīnas kārtas numurs', which renders the code below obsolete.
        # number_regex = re.compile(r"\d+")
        # df.loc[:, "dose"] = df.loc[:, "dose"].apply(lambda s: int(number_regex.search(s)[0]))

        # Rename vaccines
        self.logger.debug("Renaming vaccines.")

        def _rename_vaccine(vaccine):
            if "comirnaty" in vaccine.lower():
                return "Comirnaty", "Pfizer"
            elif "vaxzevria" in vaccine.lower():
                return "Vaxzevria", "AstraZeneca"
            elif "covishield" in vaccine.lower():
                return "Vaxzevria", "AstraZeneca"
            elif "janssen" in vaccine.lower():
                return "Janssen", "Johnson & Johnson"
            elif "spikevax" in vaccine.lower():
                return "Spikevax", "Moderna"
            elif "bbibp-corv" in vaccine.lower():
                return "BBIBP-CorV", "Sinopharm"
            elif "coronavac" in vaccine.lower():
                return "CoronaVac", "Sinovac"
            else:
                self.logger.warning("Unknown vaccine: %s", vaccine)
                return vaccine, ""

        self.data.loc[:, "vaccine"] = self.data.loc[:, "vaccine"].apply(lambda v: _rename_vaccine(v)[0])  # yapf: disable
        self.data.loc[:, "manufacturer"] = self.data.loc[:, "vaccine"].apply(lambda v: _rename_vaccine(v)[1])  # yapf: disable

        # With these columns we do not lose anything if we aggregate on a
        # daily level
        logger.debug("Computing daily aggregates by does, vaccine.")
        # yapf: disable
        self.data = (
            self.data
            .groupby(["date", "dose", "vaccine", "manufacturer"])
            .agg(count=("count", "sum"))
            .reset_index()
            .sort_values("date")
        )
        # yapf: enable
        combined = [
            self.data.loc[:, "date"].unique(),
            self.data.loc[:, "dose"].unique(),
            self.data.loc[:, "vaccine"].unique(),
            self.data.loc[:, "manufacturer"].unique(),
        ]

        df = pd.DataFrame(
            columns=["date", "dose", "vaccine", "manufacturer"],
            data=list(itertools.product(*combined)),
        )
        self.data = df.merge(self.data,
                             on=["date", "dose", "vaccine", "manufacturer"],
                             how="left").fillna(0)

        self.preprocessed = True
        return self.data

    def get_vaccination_status(self) -> pd.DataFrame:
        if not self.preprocessed:
            raise RuntimeError("Should preprocess data first.")

        df = self.data.copy()

        # Make a separate columns for each dose
        self.logger.debug("Computing daily aggregates by vaccination status.")
        total_shots = df.loc[:, "count"].sum()

        df = df.groupby(["date", "vaccine", "dose"]).agg({
            "count": "sum"
        }).reset_index()

        # Figure out if full or partial
        df.loc[:, "partial"] = None
        df.loc[:, "full"] = None

        # If two doses, always full
        df.loc[df.loc[:, "dose"] == 2, "full"] = df.loc[:, "count"]
        # If Janssen, then any number of doses is enough
        df.loc[df.loc[:, "vaccine"] == "Janssen", "full"] = df.loc[:, "count"]
        # Rest is partial
        df.loc[df.loc[:, "full"].isna(), "partial"] = df.loc[:, "count"]

        # All rows should be processed
        assert (df.loc[:, "partial"].isna() | df.loc[:, "full"].isna()).sum() == df.shape[0]  # yapf: disable

        # yapf: disable
        df = (
            df
            .groupby(["date"])
            .agg(
                partial=("partial", np.sum),
                full=("full", np.sum),
            ).reset_index()
        )
        # yapf: enable

        # No single shot should be lost
        assert total_shots == (df.loc[:, "full"].sum() +
                               df.loc[:, "partial"].sum())

        # Make sure we have all the dates
        df = df.set_index("date")
        df = df.reindex(pd.date_range(df.index.min(), df.index.max()),
                        fill_value=0)
        df = df.reset_index().rename({"index": "date"}, axis=1)

        # Fill in NAs with 0s
        df = df.fillna(0)
        return df.sort_values("date")


def main():
    "Example usage."
    v = Vaccination(path="/tmp/vaccination")
    v.download()
    v.read(memory_optimized=True)
    print(v.data.head())
    print(v.data.shape)
    v.preprocess()

    df = v.get_vaccination_status()
    print(df.head())


if __name__ == "__main__":
    main()
