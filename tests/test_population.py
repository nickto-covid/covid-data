import datetime
import os
import tempfile

import numpy as np
import pandas as pd
import pytest
from covid_data.population import Population
from covid_data.resource import LatvianOpenDataPortalResource, Resource


@pytest.fixture(scope="class")
def population():
    return Population()


class TestPopulation:

    def test_init(self):
        population = Population(tempfile.mkdtemp())
        assert isinstance(population, Population)
        assert isinstance(population, Resource)

    def test_get_last_modified(self, population):
        last_modified = population.get_last_modified()
        assert isinstance(last_modified, datetime.datetime)
        assert last_modified.tzinfo is not None

    def test_download(self, population):
        default_path = os.path.join(population.path,
                                    population.FILENAMES["source"])
        if os.path.exists(default_path):
            os.remove(default_path)

        path = population.download()
        assert path == default_path
        assert os.path.exists(path)

    def test_read(self, population):
        df = population.read()
        assert isinstance(df, pd.DataFrame)
        assert df.shape[0] > 0
        assert population.data.shape[0] == df.shape[0]

    def test_preprocess(self, population):
        assert not population.preprocessed
        df = population.preprocess()
        assert isinstance(df, pd.DataFrame)
        assert population.preprocessed

        assert isinstance(df.iloc[0].loc["date"], datetime.date)
        assert isinstance(df.iloc[0].loc["population"], np.integer)

    def test_interpolate(self, population):
        # Should be after calling .test_preprocess()
        assert population.preprocessed
        df = population.interpolate()
        assert isinstance(df, pd.DataFrame)
        assert population.data.shape[0] < df.shape[0]

        assert isinstance(df.iloc[0].loc["date"], datetime.date)
        assert isinstance(df.iloc[0].loc["population"], np.integer)
