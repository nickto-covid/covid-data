import datetime
import os
import tempfile

import pandas as pd
import pytest
from covid_data.resource import LatvianOpenDataPortalResource, Resource
from covid_data.vaccination import Vaccination


@pytest.fixture(scope="class")
def vaccination():
    return Vaccination()


class TestVaccination:

    def test_init(self):
        v = Vaccination(tempfile.mkdtemp())
        assert isinstance(v, Vaccination)
        assert isinstance(v, LatvianOpenDataPortalResource)
        assert isinstance(v, Resource)

    def test_get_metadata(self, vaccination):
        metadata = vaccination._get_metadata(
            vaccination.LV_OPEN_DATA_RESOURCE_ID)
        assert isinstance(metadata, dict)
        assert "result" in metadata.keys()

    def test_get_last_modified(self, vaccination):
        last_modified = vaccination.get_last_modified()
        assert isinstance(last_modified, datetime.datetime)
        assert last_modified.tzinfo is not None

    def test_get_urls(self, vaccination):
        urls = vaccination.get_urls()
        for url in urls:
            assert isinstance(url, str)
            assert len(url) > 0
            assert "http" in url
            assert ".xlsx" in url

    def test_download(self, vaccination):
        default_path = os.path.join(vaccination.path,
                                    vaccination.FILENAMES["source"])
        if os.path.exists(default_path):
            os.remove(default_path)

        paths = vaccination.download()
        for path in paths:
            assert os.path.exists(path)

    def test_read(self, vaccination):
        df = vaccination.read(memory_optimized=True, top=1000)
        assert isinstance(df, pd.DataFrame)
        assert df.shape[0] >= 1000
        assert vaccination.data.shape[0] >= 1000

    def test_preprocess(self, vaccination):
        assert not vaccination.preprocessed
        df = vaccination.preprocess()
        assert isinstance(df, pd.DataFrame)
        assert vaccination.preprocessed

    def test_get_vaccination_status(self, vaccination):
        df = vaccination.get_vaccination_status()
        assert isinstance(df, pd.DataFrame)
