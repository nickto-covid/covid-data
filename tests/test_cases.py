import datetime
import os
import tempfile

import pandas as pd
import pytest
from covid_data.cases import Cases
from covid_data.resource import LatvianOpenDataPortalResource, Resource


@pytest.fixture(scope="class")
def cases():
    return Cases()


class TestCases:

    def test_init(self):
        cases = Cases(tempfile.mkdtemp())
        assert isinstance(cases, Cases)
        assert isinstance(cases, LatvianOpenDataPortalResource)
        assert isinstance(cases, Resource)

    def test_get_metadata(self, cases):
        metadata = cases._get_metadata(Cases.LV_OPEN_DATA_RESOURCE_ID)
        assert isinstance(metadata, dict)
        assert "result" in metadata.keys()

    def test_get_last_modified(self, cases):
        last_modified = cases.get_last_modified()
        assert isinstance(last_modified, datetime.datetime)
        assert last_modified.tzinfo is not None

    def test_get_urls(self, cases):
        urls = cases.get_urls()
        for url in urls:
            assert isinstance(url, str)
            assert len(url) > 0
            assert "http" in url
            assert ".csv" in url

    def test_download(self, cases):
        default_path = os.path.join(cases.path, cases.FILENAMES["source"])
        if os.path.exists(default_path):
            os.remove(default_path)

        paths = cases.download()
        for path in paths:
            assert os.path.exists(path)

    def test_read(self, cases):
        df = cases.read()
        assert isinstance(df, pd.DataFrame)
        assert df.shape[0] > 0
        assert cases.data.shape[0] == df.shape[0]

    def test_preprocess(self, cases):
        assert not cases.preprocessed
        df = cases.preprocess()
        assert isinstance(df, pd.DataFrame)
        assert cases.preprocessed
