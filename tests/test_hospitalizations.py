import datetime
import os
import tempfile

import pandas as pd
import pytest
from covid_data.hospitalizations import Hospitalizations
from covid_data.resource import LatvianOpenDataPortalResource, Resource


@pytest.fixture(scope="class")
def hospitalizations():
    return Hospitalizations()


class TestHospitalizations:

    def test_init(self):
        hospitalizations = Hospitalizations(tempfile.mkdtemp())
        assert isinstance(hospitalizations, Hospitalizations)
        assert isinstance(hospitalizations, LatvianOpenDataPortalResource)
        assert isinstance(hospitalizations, Resource)

    def test_get_metadata(self, hospitalizations):
        metadata = hospitalizations._get_metadata(
            Hospitalizations.LV_OPEN_DATA_RESOURCE_ID)
        assert isinstance(metadata, dict)
        assert "result" in metadata.keys()

    def test_get_last_modified(self, hospitalizations):
        last_modified = hospitalizations.get_last_modified()
        assert isinstance(last_modified, datetime.datetime)
        assert last_modified.tzinfo is not None

    def test_get_url(self, hospitalizations):
        urls = hospitalizations.get_urls()
        for url in urls:
            assert isinstance(url, str)
            assert len(url) > 0
            assert "http" in url
            assert ".xlsx" in url

    def test_download(self, hospitalizations):
        default_path = os.path.join(hospitalizations.path,
                                    hospitalizations.FILENAMES["source"])
        if os.path.exists(default_path):
            os.remove(default_path)

        paths = hospitalizations.download()
        for path in paths:
            assert os.path.exists(path)

    def test_read(self, hospitalizations):
        df = hospitalizations.read()
        assert isinstance(df, pd.DataFrame)
        assert df.shape[0] > 0
        assert hospitalizations.data.shape[0] == df.shape[0]

    def test_preprocess(self, hospitalizations):
        assert not hospitalizations.preprocessed
        df = hospitalizations.preprocess()
        assert isinstance(df, pd.DataFrame)
        assert hospitalizations.preprocessed
