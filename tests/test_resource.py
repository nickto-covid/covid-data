import pandas as pd
import pytest
from covid_data.resource import Resource

FILEPATH = "tests/assets/sample_document.xlsx"
SHEET_NAME = "Sheet1"


class TestMemoryOptimizedRead:

    def test_same_content(self):
        pandas_df = pd.read_excel(FILEPATH, sheet_name=SHEET_NAME)
        custom_df = Resource._read_excel_memory_optimized(FILEPATH,
                                                          sheet=SHEET_NAME)
        assert pandas_df.equals(custom_df)

    def test_columns(self):
        columns = ["column1", "column3"]
        df = Resource._read_excel_memory_optimized(FILEPATH,
                                                   sheet=SHEET_NAME,
                                                   columns=columns)
        assert df.shape == (5, 2)
        assert [c in df.columns for c in columns]
        assert "column2" not in df.columns

    def test_top(self):
        df = Resource._read_excel_memory_optimized(FILEPATH,
                                                   sheet=SHEET_NAME,
                                                   top=3)
        assert df.shape == (3, 3)

    def test_columns_and_top(self):
        columns = ["column1", "column3"]
        df = Resource._read_excel_memory_optimized(FILEPATH,
                                                   sheet=SHEET_NAME,
                                                   columns=columns,
                                                   top=3)
        assert df.shape == (3, 2)
        assert [c in df.columns for c in columns]
        assert "column2" not in df.columns

    def test_sheet(self):
        # Non existing
        with pytest.raises(KeyError):
            df = Resource._read_excel_memory_optimized(FILEPATH,
                                                       sheet="NotExistingSheet")

        # Existing
        df = Resource._read_excel_memory_optimized(FILEPATH, sheet="Sheet2")
        assert isinstance(df, pd.DataFrame)
        assert df.shape == (5, 3)
